# RobotOpenCV

<img src="https://gitlab.com/FrVi/RobotOpenCV/raw/master/images/all.png" alt="Drawing" width="1000"/> 
<br />



This project is taking an image (from a camera) as input and asks the user to calibrate filters.

It was made for the robot club of Supelec in 2011 and is using OpenCV version 2.1.

My role in the team was to work on image processing and artificial intelligence.

We participated to the Eurobot competition, which has a different rule every year.

<br />
## Rules

The goal in 2011 was to push yellow pucks on your colour (red or blue).

There were 3 types of pucks with increasing values:
- Paws
- Queens
- Kings

<br />
Robots were allowed to put up two paws under a Queen or a King to increase the value of it.

The table was made of red/blue squares and pucks were yellow with a special pattern on the side of queens/kings.

Thanks to this, it was easier to find positions of objects.

Our robot had a clamp that it could open/close and move vertically to lift Queens/Kings.

<br />
## The program

It uses simple thresholds filters, erosion, dilation, Hough transform and Circle Hough transform to find pucks and board lines.

The values are set by the user in several steps.

If calibrated correctly it reports 3D coordinates and nature of the objects in the scene.

The output of this program was made to be later used by the robot to find objects as it moves on the table.

<br />
## Commands

The objective of each step is written in the console.

Press **Enter** to go to next step after you are satisfied with the filters values.
