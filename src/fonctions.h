#ifndef FONCTIONS_H
#define FONCTIONS_H

#include <stdlib.h> 
#include <stdio.h> 
#include <OpenCV2.1/highgui.h>
#include <OpenCV2.1/cv.h>
#include <vector>

typedef struct Coord3D Coord3D;
struct Coord3D
{
    double x;
    double y;
	double z;
};

typedef struct Droite Droite;
struct Droite
{
	double a;
	double b;
};

typedef struct Tache Tache;
struct Tache
{
    int x_centre;
    int y_centre;
	int surface;

	int x_min;
	int y_min;
    int x_max;
	int y_max;

	CvSeq* contour;
};

using namespace std;

CvPoint calculer_coord_ecran(double x, double y, double z,int pos_x, int pos_y, int pos_z, double theta, double phi);
Coord3D calculer_position_initiale(bool est_rouge, double x, double y, double z, double x_ecran, double y_ecran, double pos_z, double phi);
Coord3D calculer_coord_espace(double x_ecran, double y_ecran,int pos_x, int pos_y, int pos_z, double theta, double phi, double z_postule);
void afficher(CvArr* img, int pos_x, int pos_y, int pos_z, double theta, double phi,Coord3D tab[7][6]);
bool pointIsInsideContour(CvSeq *contour, int x, int y);
Tache calcul_contour(CvSeq* contour,IplImage* dst, int x, int y);
bool AssezProche(int a, int b);
bool Proche(int a, int b);
void AfficherContour(IplImage* dst,CvSeq* seq,CvScalar color);
void Recherche_Coord_Pion(CvPoint A, CvPoint B,int pos_x, int pos_y, int pos_z, double theta, double phi, double c_hori, double c_vert);
double Dist2(double x1, double y1, double x2, double y2);
double Dist_droite2(double x, double y,double a, double b, double c);
double Dist_segment2(double x, double y,double x1, double y1, double x2, double y2);
CvPoint Intersection_droites(Droite d1, Droite d2);
double Angle(Droite d);
bool Meme_Angle(Droite d1, Droite d2);

#endif
