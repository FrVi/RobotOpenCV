

/*
void Hough(IplImage* src)
{

    IplImage* dst = cvCreateImage( cvGetSize(src), 8, 1 ); 
    IplImage* color_dst = cvCreateImage( cvGetSize(src), 8, 3 ); 
    CvMemStorage* storage = cvCreateMemStorage(0); 
    CvSeq* lines = 0; 
    int i; 

    cvCanny( src, dst, 50, 200, 3 ); 
    cvCvtColor( dst, color_dst, CV_GRAY2BGR );

    lines = cvHoughLines2( dst, storage, CV_HOUGH_STANDARD, 1, CV_PI/180, 100, 0, 0 ); 

    for( i = 0; i < MIN(lines->total,100); i++ ) 
    { 
        float* line = (float*)cvGetSeqElem(lines,i); 
        float rho = line[0]; 
        float theta = line[1]; 
        CvPoint pt1, pt2; 
        double a = cos(theta), b = sin(theta); 
        double x0 = a*rho, y0 = b*rho; 
        pt1.x = cvRound(x0 + 1000*(-b)); 
        pt1.y = cvRound(y0 + 1000*(a)); 
        pt2.x = cvRound(x0 - 1000*(-b)); 
        pt2.y = cvRound(y0 - 1000*(a)); 
        cvLine( color_dst, pt1, pt2, CV_RGB(255,0,0)); 
     } 

     cvNamedWindow( "Source", 1 ); 
     cvShowImage( "Source", src ); 

     cvNamedWindow( "Hough", 1 ); 
     cvShowImage( "Hough", color_dst ); 
		
     cvReleaseImage(&src);
	 cvReleaseImage(&color_dst);
}



*/