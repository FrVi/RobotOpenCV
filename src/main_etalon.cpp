#include "fonctions.h" 

IplImage* img_pion1;
IplImage* img_pion2;
IplImage* img_hough;

IplImage* img_nvg_blue;
IplImage* img_nvg_red;
IplImage* img_nvg_green;
IplImage* img_bin_blue;
IplImage* img_bin_red;
IplImage* img_bin_green;

IplImage* img_nvg_hough;
IplImage* img_bin_hough;

IplImage* temp;
IplImage* temp2;
IplImage* temp3;

int bleu_r_h=133;
int bleu_b_h=111;
int bleu_g_h=52;

int rouge_r_h=186;
int rouge_b_h=152;
int rouge_g_h=146;

int jaune_r_h=256;
int jaune_r_l=210;

int jaune_g_h=256;
int jaune_g_l=201;

int jaune_b_h=223;
int jaune_b_l=117;

int blanc_r_h=200;
int blanc_r_l=101;

int blanc_g_h=203;
int blanc_g_l=109;

int blanc_b_h=205;
int blanc_b_l=91;

int noir_r_h=154;
int noir_b_h=32;
int noir_g_h=109;

FILE* fichier = NULL;

void seuillage_blue_haut(int valeur)
{

    cvThreshold(img_nvg_blue, img_bin_blue, valeur, 255, CV_THRESH_BINARY_INV);
    cvShowImage("Blue comp", img_bin_blue);


	cvAnd(img_bin_blue, img_bin_red, temp, NULL);
	cvAnd(img_bin_green, temp, temp2, NULL);

	/*cvNamedWindow( "temp", 1 ); 
    cvShowImage( "temp", temp2 );*/

	IplConvKernel *noyau=cvCreateStructuringElementEx(3, 3, 1, 1, CV_SHAPE_RECT);

	cvErode(temp2, temp, noyau);//l'ouverture n'est pas egale � la compos�e des 2, je ne sais pas pk
	cvDilate(temp, temp3, noyau);
	cvNamedWindow( "Result", 1 ); 
    cvShowImage( "Result", temp3 );

}

void seuillage_blue_bas(int valeur)
{

    cvThreshold(img_nvg_blue, img_bin_blue, valeur, 255, CV_THRESH_BINARY);
    cvShowImage("Blue comp", img_bin_blue);


	cvAnd(img_bin_blue, img_bin_red, temp, NULL);
	cvAnd(img_bin_green, temp, temp2, NULL);

	/*cvNamedWindow( "temp", 1 ); 
    cvShowImage( "temp", temp2 );*/

	IplConvKernel *noyau=cvCreateStructuringElementEx(3, 3, 1, 1, CV_SHAPE_RECT);

	cvErode(temp2, temp, noyau);//l'ouverture n'est pas egale � la compos�e des 2, je ne sais pas pk
	cvDilate(temp, temp3, noyau);
	cvNamedWindow( "Result", 1 ); 
    cvShowImage( "Result", temp3 );

}

void seuillage_red_haut(int valeur)
{

    cvThreshold(img_nvg_red, img_bin_red, valeur, 255, CV_THRESH_BINARY_INV);
    cvShowImage("Red comp", img_bin_red);

	cvAnd(img_bin_red, img_bin_blue, temp, NULL);
	cvAnd(img_bin_green, temp, temp2, NULL);

	/*cvNamedWindow( "temp", 1 ); 
    cvShowImage( "temp", temp2 );*/

	IplConvKernel *noyau=cvCreateStructuringElementEx(3, 3, 1, 1, CV_SHAPE_RECT);

	cvErode(temp2, temp, noyau);//l'ouverture n'est pas egale � la compos�e des 2, je ne sais pas pk
	cvDilate(temp, temp3, noyau);
	cvNamedWindow( "Result", 1 ); 
    cvShowImage( "Result", temp3 );
}

void seuillage_red_bas(int valeur)
{

    cvThreshold(img_nvg_red, img_bin_red, valeur, 255, CV_THRESH_BINARY);
    cvShowImage("Red comp", img_bin_red);

	cvAnd(img_bin_red, img_bin_blue, temp, NULL);
	cvAnd(img_bin_green, temp, temp2, NULL);

	/*cvNamedWindow( "temp", 1 ); 
    cvShowImage( "temp", temp2 );*/

	IplConvKernel *noyau=cvCreateStructuringElementEx(3, 3, 1, 1, CV_SHAPE_RECT);

	cvErode(temp2, temp, noyau);//l'ouverture n'est pas egale � la compos�e des 2, je ne sais pas pk
	cvDilate(temp, temp3, noyau);
	cvNamedWindow( "Result", 1 ); 
    cvShowImage( "Result", temp3 );
}


void seuillage_green_haut(int valeur)
{

    cvThreshold(img_nvg_green, img_bin_green, valeur, 255, CV_THRESH_BINARY_INV);
    cvShowImage("Green comp", img_bin_green);

	cvAnd(img_bin_green, img_bin_blue, temp, NULL);
	cvAnd(img_bin_red, temp, temp2, NULL);

	/*cvNamedWindow( "temp", 1 ); 
    cvShowImage( "temp", temp2 );*/

	IplConvKernel *noyau=cvCreateStructuringElementEx(3, 3, 1, 1, CV_SHAPE_RECT);

	cvErode(temp2, temp, noyau);//l'ouverture n'est pas egale � la compos�e des 2, je ne sais pas pk
	cvDilate(temp, temp3, noyau);
	cvNamedWindow( "Result", 1 ); 
    cvShowImage( "Result", temp3 );
}

void seuillage_green_bas(int valeur)
{

    cvThreshold(img_nvg_green, img_bin_green, valeur, 255, CV_THRESH_BINARY);
    cvShowImage("Green comp", img_bin_green);

	cvAnd(img_bin_green, img_bin_blue, temp, NULL);
	cvAnd(img_bin_red, temp, temp2, NULL);

	/*cvNamedWindow( "temp", 1 ); 
    cvShowImage( "temp", temp2 );*/

	IplConvKernel *noyau=cvCreateStructuringElementEx(3, 3, 1, 1, CV_SHAPE_RECT);

	cvErode(temp2, temp, noyau);//l'ouverture n'est pas egale � la compos�e des 2, je ne sais pas pk
	cvDilate(temp, temp3, noyau);
	cvNamedWindow( "Result", 1 ); 
    cvShowImage( "Result", temp3 );
}

int rho=1;
int threshold=19;

void seuillage_hough(int valeur)
{
    CvMemStorage* storage = cvCreateMemStorage(0); 
    CvSeq* lines = 0; 
    int i; 

    cvCanny(img_nvg_hough, img_bin_hough, 10, 200, 3 ); 
	lines = cvHoughLines2(img_bin_hough, storage, CV_HOUGH_PROBABILISTIC, max(rho,1), CV_PI/360, max(threshold,4), 0, 0 ); 

	cvReleaseImage(&img_hough);
	img_hough=cvCreateImage( cvGetSize(img_nvg_hough), 8, 3 );

    for( i = 0; i < MIN(lines->total,100); i++ ) 
    { 
        CvPoint* line = (CvPoint*)cvGetSeqElem(lines,i); 
        CvPoint pt1=line[0];
		CvPoint	pt2=line[1]; 

        cvLine( img_hough, pt1, pt2, CV_RGB(255,0,0)); 
     } 


	cvNamedWindow( "Hough Calibration", 1 ); 
    cvShowImage( "Hough Calibration", img_hough );
}

void etalonnage_noir(IplImage* red, IplImage* green, IplImage* blue, IplImage* dest)
{
	IplImage* red3=cvCreateImage( cvGetSize(red), red->depth,1 );
	IplImage* blue3=cvCreateImage( cvGetSize(red), red->depth,1 );
	IplImage* green3=cvCreateImage( cvGetSize(red), red->depth,1 );

	temp=cvCreateImage( cvGetSize(red), red->depth,1 );
	temp2=cvCreateImage( cvGetSize(red), red->depth,1 );
	temp3=cvCreateImage( cvGetSize(red), red->depth,1 );

	cvThreshold( red, red3,  noir_r_h, 255,CV_THRESH_BINARY);
	cvThreshold( blue, blue3, noir_b_h, 255,CV_THRESH_BINARY);
	cvThreshold( green, green3, noir_g_h, 255,CV_THRESH_BINARY);

	 cvNamedWindow( "red", 1 ); 
     cvShowImage( "red", red);

	 cvNamedWindow( "blue", 1 ); 
     cvShowImage( "blue", blue); 
		
 	 cvNamedWindow( "green", 1 ); 
     cvShowImage( "green", green); 

	 cvNamedWindow( "red3", 1 ); 
     cvShowImage( "red3", red3 ); 

	 cvNamedWindow( "blue3", 1 ); 
     cvShowImage( "blue3", blue3 ); 
		
 	 cvNamedWindow( "green3", 1 ); 
     cvShowImage( "green3", green3 ); 

	 img_bin_blue=cvCreateImage( cvGetSize(blue3), blue3->depth,1 );
	 img_bin_blue=cvCloneImage(blue3);
	 img_bin_red=cvCreateImage( cvGetSize(red3), red3->depth,1 );
	 img_bin_red=cvCloneImage(red3);
	 img_bin_green=cvCreateImage( cvGetSize(green3), green3->depth,1 );
	 img_bin_green=cvCloneImage(green3);


	cvAnd(red3, green3, temp, NULL);
	cvAnd(blue3, temp, temp2, NULL);

	cvNamedWindow( "temp", 1 ); 
    cvShowImage( "temp", temp2 );

IplConvKernel *noyau=cvCreateStructuringElementEx(3, 3, 1, 1, CV_SHAPE_RECT);

	cvErode(temp2, temp, noyau);//l'ouverture n'est pas egale � la compos�e des 2, je ne sais pas pk
	cvDilate(temp, temp3, noyau);
	cvNamedWindow( "temp3", 1 ); 
    cvShowImage( "temp3", temp3 );




	 //cvMerge(blue2,green2,red2,NULL,dest);
	 img_nvg_blue=cvCreateImage( cvGetSize(red), red->depth,1 );
	 img_nvg_blue=cvCloneImage(blue);
	 img_nvg_red=cvCreateImage( cvGetSize(red), red->depth,1 );
	 img_nvg_red=cvCloneImage(red);
	 img_nvg_green=cvCreateImage( cvGetSize(red), red->depth,1 );
	 img_nvg_green=cvCloneImage(green);

cvCreateTrackbar( "Thres blue", "blue3", &noir_b_h , 255 , seuillage_blue_haut );
cvCreateTrackbar( "Thres green", "green3", &noir_g_h , 255 , seuillage_green_haut );
cvCreateTrackbar( "Thres red", "red3", &noir_r_h , 255 , seuillage_red_haut );

cvCopyImage(temp3,dest);

    cvWaitKey(0);

     cvReleaseImage(&img_nvg_blue);
	 cvReleaseImage(&img_nvg_red);
	 cvReleaseImage(&img_nvg_green);

     cvReleaseImage(&img_bin_blue);
	 cvReleaseImage(&img_bin_red);
	 cvReleaseImage(&img_bin_green);

     cvReleaseImage(&red3);
	 cvReleaseImage(&blue3);
	 cvReleaseImage(&green3);

	 cvReleaseImage(&temp);
	  cvReleaseImage(&temp2);
	   cvReleaseImage(&temp3);
}



void etalonnage_rouge(IplImage* red, IplImage* green, IplImage* blue, IplImage* dest)
{
	//printf("Reglez les seuils pour que la case rouge qui sert de repere ait des bords droits\n");
	printf("Chooses the thresholds so that the nearest red square has straight edges in the result window.\n");
	IplImage* red3=cvCreateImage( cvGetSize(red), red->depth,1 );
	IplImage* blue3=cvCreateImage( cvGetSize(red), red->depth,1 );
	IplImage* green3=cvCreateImage( cvGetSize(red), red->depth,1 );

	temp=cvCreateImage( cvGetSize(red), red->depth,1 );
	temp2=cvCreateImage( cvGetSize(red), red->depth,1 );
	temp3=cvCreateImage( cvGetSize(red), red->depth,1 );

	cvThreshold( red, red3,  rouge_r_h, 255,CV_THRESH_BINARY);
	cvThreshold( blue, blue3, rouge_b_h, 255,CV_THRESH_BINARY_INV);
	cvThreshold( green, green3, rouge_g_h, 255,CV_THRESH_BINARY_INV);

	cvNamedWindow( "Red comp", 1 ); 
    cvShowImage( "Red comp", red3 ); 

	cvNamedWindow( "Blue comp", 1 ); 
    cvShowImage( "Blue comp", blue3 ); 
		
 	cvNamedWindow( "Green comp", 1 ); 
    cvShowImage( "Green comp", green3 ); 

	img_bin_blue=cvCreateImage( cvGetSize(blue3), blue3->depth,1 );
	img_bin_blue=cvCloneImage(blue3);
	img_bin_red=cvCreateImage( cvGetSize(red3), red3->depth,1 );
	img_bin_red=cvCloneImage(red3);
	img_bin_green=cvCreateImage( cvGetSize(green3), green3->depth,1 );
	img_bin_green=cvCloneImage(green3);

	cvAnd(red3, green3, temp, NULL);
	cvAnd(blue3, temp, temp2, NULL);

	IplConvKernel *noyau=cvCreateStructuringElementEx(3, 3, 1, 1, CV_SHAPE_RECT);

	cvErode(temp2, temp, noyau);//l'ouverture n'est pas egale � la compos�e des 2, je ne sais pas pk
	cvDilate(temp, temp3, noyau);
	cvNamedWindow( "Result", 1 ); 
    cvShowImage( "Result", temp3 );

	img_nvg_blue=cvCreateImage( cvGetSize(red), red->depth,1 );
	img_nvg_blue=cvCloneImage(blue);
	img_nvg_red=cvCreateImage( cvGetSize(red), red->depth,1 );
	img_nvg_red=cvCloneImage(red);
	img_nvg_green=cvCreateImage( cvGetSize(red), red->depth,1 );
	img_nvg_green=cvCloneImage(green);

cvCreateTrackbar( "Thres bleu", "Blue comp", &rouge_b_h , 255 , seuillage_blue_haut );
cvCreateTrackbar( "Thres green", "Green comp", &rouge_g_h , 255 , seuillage_green_haut );
cvCreateTrackbar( "Thres red", "Red comp", &rouge_r_h , 255 , seuillage_red_bas );

cvCopyImage(temp3,dest);

    cvWaitKey(0);

	cvThreshold( red, red3,  rouge_r_h, 255,CV_THRESH_BINARY);
	cvThreshold( blue, blue3, rouge_b_h, 255,CV_THRESH_BINARY_INV);
	cvThreshold( green, green3, rouge_g_h, 255,CV_THRESH_BINARY_INV);

	cvAnd(red3, green3, temp, NULL);
	cvAnd(blue3, temp, temp2, NULL);

	cvErode(temp2, temp, noyau);
	cvDilate(temp, temp3, noyau);
	cvCopyImage(temp3,dest);

	printf("chosen thresholds (rouge): %ld %ld %ld (r g b)\n\n\n", rouge_r_h,rouge_g_h,rouge_b_h);
	fprintf(fichier, "r %ld %ld %ld\n", rouge_r_h,rouge_g_h,rouge_b_h);

	cvDestroyWindow( "Result" );
	cvDestroyWindow( "Red comp" );
	cvDestroyWindow( "Green comp" );
	cvDestroyWindow( "Blue comp" );

     cvReleaseImage(&img_nvg_blue);
	 cvReleaseImage(&img_nvg_red);
	 cvReleaseImage(&img_nvg_green);

     cvReleaseImage(&img_bin_blue);
	 cvReleaseImage(&img_bin_red);
	 cvReleaseImage(&img_bin_green);

     cvReleaseImage(&red3);
	 cvReleaseImage(&blue3);
	 cvReleaseImage(&green3);

	 cvReleaseImage(&temp);
	  cvReleaseImage(&temp2);
	   cvReleaseImage(&temp3);
}


void seuillage_jaune(int valeur)
{
cvDestroyWindow( "Source" );
cvDestroyWindow( "Hough" );

cvInRangeS(img_pion1, CV_RGB(jaune_r_l,jaune_g_l,jaune_b_l),CV_RGB(jaune_r_h,jaune_g_h,jaune_b_h), img_pion2);
cvNamedWindow( "Paw", 1 ); 
cvShowImage( "Paw", img_pion2);

IplImage* temp=cvCreateImage( cvGetSize(img_pion2), img_pion2->depth,1 );
IplImage* temp2=cvCreateImage( cvGetSize(img_pion2), img_pion2->depth,1 );

//IplConvKernel* noyau = cvCreateStructuringElementEx(15, 15, 7, 7, CV_SHAPE_RECT);
IplConvKernel* noyau = cvCreateStructuringElementEx(5, 5, 2, 2, CV_SHAPE_RECT);
cvMorphologyEx(img_pion2, temp2, temp, noyau, CV_MOP_OPEN);

cvNamedWindow( "Paw opening (intermediate)", 1 ); 
cvShowImage( "Paw opening (intermediate)", temp2);

//IplConvKernel* noyau2 = cvCreateStructuringElementEx(19, 19, 9, 9, CV_SHAPE_RECT);
IplConvKernel* noyau2 = cvCreateStructuringElementEx(5, 5, 2, 2, CV_SHAPE_RECT);
cvDilate(temp2, temp3, noyau2);


cvNamedWindow( "Paw dilation (result)", 1 ); 
cvShowImage( "Paw dilation (result)", temp3);

cvReleaseImage(&temp);
cvReleaseImage(&temp2);

}

void etalonnage_jaune(IplImage* src, IplImage* dest)
{
img_pion1=cvCreateImage( cvGetSize(src), src->depth,1 );
img_pion2=cvCreateImage( cvGetSize(src), src->depth,1 );
img_pion1=cvCloneImage(src);

CvScalar min_color = CV_RGB(jaune_r_l,jaune_g_l,jaune_b_l);
CvScalar max_color = CV_RGB(jaune_r_h,jaune_g_h,jaune_b_h);
cvInRangeS(src, min_color,max_color, dest);//search for the color in image

printf("Choose the thresholds so that the top of each paw is white and the rest is as dark as possible\n");
//printf("Choisissez les seuils pour que les sommets des pions soient blancs et le reste noir ou pas trop blanc\n");

	cvNamedWindow( "Paw", 1 ); 
    cvShowImage( "Paw", dest);

	temp3=cvCreateImage( cvGetSize(src), src->depth,1 );
	cvCopyImage(dest,temp3);

cvCreateTrackbar( "Thres b h", "Paw", &jaune_b_h , 256 , seuillage_jaune);
cvCreateTrackbar( "thres g h", "Paw", &jaune_g_h , 256 , seuillage_jaune);
cvCreateTrackbar( "Thres r h", "Paw", &jaune_r_h , 256 , seuillage_jaune);

cvCreateTrackbar( "Thres b l", "Paw", &jaune_b_l , 256 , seuillage_jaune);
cvCreateTrackbar( "thres g l", "Paw", &jaune_g_l , 256 , seuillage_jaune);
cvCreateTrackbar( "Thres r l", "Paw", &jaune_r_l , 256 , seuillage_jaune);

cvWaitKey(0);
printf("chosen thresholds : %ld %ld %ld %ld %ld %ld (rh, rl, gh, gl, bh, bl)\n\n\n",jaune_r_h,jaune_r_l,jaune_g_h,jaune_g_l,jaune_b_h,jaune_b_l);
fprintf(fichier,"chosen thresholds (j) : %ld %ld %ld %ld %ld %ld (rh, rl, gh, gl, bh, bl)\n",jaune_r_h,jaune_r_l,jaune_g_h,jaune_g_l,jaune_b_h,jaune_b_l);
cvDestroyWindow( "Paw" );
cvDestroyWindow( "Paw opening (intermediate)" );
cvDestroyWindow( "Paw dilation (result)" );

/*
cvReleaseImage(&img_pion1);
cvReleaseImage(&img_pion2);*/

}

void seuillage_blanc(int valeur)
{
cvDestroyWindow( "Source" );
cvDestroyWindow( "Hough" );

cvInRangeS(img_pion1, CV_RGB(blanc_r_l,blanc_g_l,blanc_b_l),CV_RGB(blanc_r_h,blanc_g_h,blanc_b_h), img_pion2);
cvNamedWindow( "Paw", 1 ); 
cvShowImage( "Paw", img_pion2);

IplImage* temp=cvCreateImage( cvGetSize(img_pion2), img_pion2->depth,1 );
IplImage* temp2=cvCreateImage( cvGetSize(img_pion2), img_pion2->depth,1 );

//IplConvKernel* noyau = cvCreateStructuringElementEx(15, 15, 7, 7, CV_SHAPE_RECT);
IplConvKernel* noyau = cvCreateStructuringElementEx(5, 5, 2, 2, CV_SHAPE_RECT);
cvMorphologyEx(img_pion2, temp2, temp, noyau, CV_MOP_OPEN);

cvNamedWindow( "Paw opening (intermediate)", 1 ); 
cvShowImage( "Paw opening (intermediate)", temp2);

//IplConvKernel* noyau2 = cvCreateStructuringElementEx(19, 19, 9, 9, CV_SHAPE_RECT);
IplConvKernel* noyau2 = cvCreateStructuringElementEx(5, 5, 2, 2, CV_SHAPE_RECT);
cvDilate(temp2, temp3, noyau2);


cvNamedWindow( "Paw dilation (result)", 1 ); 
cvShowImage( "Paw dilation (result)", temp3);

cvReleaseImage(&temp);
cvReleaseImage(&temp2);

}

void etalonnage_blanc(IplImage* src, IplImage* dest)
{
img_pion1=cvCreateImage( cvGetSize(src), src->depth,1 );
img_pion2=cvCreateImage( cvGetSize(src), src->depth,1 );
img_pion1=cvCloneImage(src);

CvScalar min_color = CV_RGB(jaune_r_l,jaune_g_l,jaune_b_l);
CvScalar max_color = CV_RGB(jaune_r_h,jaune_g_h,jaune_b_h);
cvInRangeS(src, min_color,max_color, dest);//search for the color in image

printf("Choose the thresholds so that the bases of the kings/queens are white and the rest is as dark as possible.\n");
//printf("Choisissez les seuils pour que les bases des rois/reines soient blancs et le reste noir ou pas trop blanc\n");

	cvNamedWindow( "Paw", 1 ); 
    cvShowImage( "Paw", dest);

	temp3=cvCreateImage( cvGetSize(src), src->depth,1 );
	cvCopyImage(dest,temp3);

cvCreateTrackbar( "Thres b h", "Paw", &blanc_b_h , 256 , seuillage_blanc);
cvCreateTrackbar( "thres g h", "Paw", &blanc_g_h , 256 , seuillage_blanc);
cvCreateTrackbar( "Thres r h", "Paw", &blanc_r_h , 256 , seuillage_blanc);

cvCreateTrackbar( "Thres b l", "Paw", &blanc_b_l , 256 , seuillage_blanc);
cvCreateTrackbar( "thres g l", "Paw", &blanc_g_l , 256 , seuillage_blanc);
cvCreateTrackbar( "Thres r l", "Paw", &blanc_r_l , 256 , seuillage_blanc);

cvWaitKey(0);
printf("chosen thresholds : %ld %ld %ld %ld %ld %ld (rh, rl, gh, gl, bh, bl)\n\n\n",blanc_r_h,blanc_r_l,blanc_g_h,blanc_g_l,blanc_b_h,blanc_b_l);
fprintf(fichier,"chosen thresholds (b) : %ld %ld %ld %ld %ld %ld (rh, rl, gh, gl, bh, bl)\n",blanc_r_h,blanc_r_l,blanc_g_h,blanc_g_l,blanc_b_h,blanc_b_l);
cvDestroyWindow( "Paw" );
cvDestroyWindow( "Paw opening (intermediate)" );
cvDestroyWindow( "Paw dilation (result)" );

/*
cvReleaseImage(&img_pion1);
cvReleaseImage(&img_pion2);*/

}

void etalonnage_bleu(IplImage* red, IplImage* green, IplImage* blue, IplImage* dest)
{
	IplImage* red3=cvCreateImage( cvGetSize(red), red->depth,1 );
	IplImage* blue3=cvCreateImage( cvGetSize(red), red->depth,1 );
	IplImage* green3=cvCreateImage( cvGetSize(red), red->depth,1 );

	temp=cvCreateImage( cvGetSize(red), red->depth,1 );
	temp2=cvCreateImage( cvGetSize(red), red->depth,1 );
	temp3=cvCreateImage( cvGetSize(red), red->depth,1 );

	cvThreshold( red, red3,  bleu_r_h, 255,CV_THRESH_BINARY_INV);
	cvThreshold( blue, blue3, bleu_b_h, 255,CV_THRESH_BINARY);
	cvThreshold( green, green3, bleu_g_h, 255,CV_THRESH_BINARY);

	//img_test=cvCloneImage(img);
	//cvMerge(temp,img_test2,temp,NULL,img_test);

	//cvShowImage("test", img_test);

	//cvThreshold( green, temp, 120, 255,CV_THRESH_BINARY);
	//cvThreshold( red, img_test2, 120, 255,CV_THRESH_BINARY);

	 /*cvNamedWindow( "red", 1 ); 
     cvShowImage( "red", red);

	 cvNamedWindow( "blue", 1 ); 
     cvShowImage( "blue", blue); 
		
 	 cvNamedWindow( "green", 1 ); 
     cvShowImage( "green", green); */

	 cvNamedWindow( "Red comp", 1 ); 
     cvShowImage( "Red comp", red3 ); 

	 cvNamedWindow( "Blue comp", 1 ); 
     cvShowImage( "Blue comp", blue3 ); 
		
 	 cvNamedWindow( "Green comp", 1 ); 
     cvShowImage( "Green comp", green3 );

	 img_bin_blue=cvCreateImage( cvGetSize(blue3), blue3->depth,1 );
	 img_bin_blue=cvCloneImage(blue3);
	 img_bin_red=cvCreateImage( cvGetSize(red3), red3->depth,1 );
	 img_bin_red=cvCloneImage(red3);
	 img_bin_green=cvCreateImage( cvGetSize(green3), green3->depth,1 );
	 img_bin_green=cvCloneImage(green3);


	cvAnd(red3, green3, temp, NULL);
	cvAnd(blue3, temp, temp2, NULL);

	/*cvNamedWindow( "temp", 1 ); 
    cvShowImage( "temp", temp2 );*/

IplConvKernel *noyau=cvCreateStructuringElementEx(3, 3, 1, 1, CV_SHAPE_RECT);

	cvErode(temp2, temp, noyau);//l'ouverture n'est pas egale � la compos�e des 2, je ne sais pas pk
	cvDilate(temp, temp3, noyau);

	cvNamedWindow( "Result", 1 ); 
    cvShowImage( "Result", temp3 );




	 //cvMerge(blue2,green2,red2,NULL,dest);
	 img_nvg_blue=cvCreateImage( cvGetSize(red), red->depth,1 );
	 img_nvg_blue=cvCloneImage(blue);
	 img_nvg_red=cvCreateImage( cvGetSize(red), red->depth,1 );
	 img_nvg_red=cvCloneImage(red);
	 img_nvg_green=cvCreateImage( cvGetSize(red), red->depth,1 );
	 img_nvg_green=cvCloneImage(green);

cvCreateTrackbar( "Blue threshold", "Blue comp", &bleu_b_h , 255 , seuillage_blue_bas );
cvCreateTrackbar( "Green threshold", "Green comp", &bleu_g_h , 255 , seuillage_green_bas );
cvCreateTrackbar( "Red threshold", "Red comp", &bleu_r_h , 255 , seuillage_red_haut );

cvCopyImage(temp3,dest);

    cvWaitKey(0);

	cvDestroyWindow( "Result" );
	cvDestroyWindow( "Red comp" );
	cvDestroyWindow( "Green comp" );
	cvDestroyWindow( "Blue comp" );

     cvReleaseImage(&img_nvg_blue);
	 cvReleaseImage(&img_nvg_red);
	 cvReleaseImage(&img_nvg_green);

     cvReleaseImage(&img_bin_blue);
	 cvReleaseImage(&img_bin_red);
	 cvReleaseImage(&img_bin_green);

     cvReleaseImage(&red3);
	 cvReleaseImage(&blue3);
	 cvReleaseImage(&green3);

	 cvReleaseImage(&temp);
	  cvReleaseImage(&temp2);
	   cvReleaseImage(&temp3);
}




void Hough_line_etalonnage(IplImage* src)
{

    IplImage* dst = cvCreateImage( cvGetSize(src), 8, 1 ); 
    IplImage* color_dst = cvCreateImage( cvGetSize(src), 8, 3 ); 
    CvMemStorage* storage = cvCreateMemStorage(0); 
    CvSeq* lines = 0; 
    int i; 

    cvCanny( src, dst, 10, 200, 3 ); 
    //cvCvtColor( dst, color_dst, CV_GRAY2BGR );

	lines = cvHoughLines2( dst, storage, CV_HOUGH_PROBABILISTIC, rho, CV_PI/360, threshold, 0, 0 ); 

    for( i = 0; i < MIN(lines->total,100); i++ ) 
    { 
        CvPoint* line = (CvPoint*)cvGetSeqElem(lines,i); 
        CvPoint pt1=line[0];
		CvPoint	pt2=line[1]; 

        cvLine( color_dst, pt1, pt2, CV_RGB(255,0,0)); 
     } 

     cvNamedWindow( "Source", 1 ); 
     cvShowImage( "Source", src ); 

	 printf("Choose the thresholds so that the intersection used as mark is well-positioned.\n");
	 //printf("Reglez les seuils pour que l'intersection qui sert de repere soit bien reperee\n");
     cvNamedWindow( "Hough Calibration", 1 ); 
     cvShowImage( "Hough Calibration", color_dst ); 
		
     cvReleaseImage(&dst);
	 cvReleaseImage(&color_dst);


	 img_nvg_hough=cvCreateImage( cvGetSize(src), src->depth,1 );
	 img_nvg_hough=cvCloneImage(src);

	 img_bin_hough=cvCreateImage( cvGetSize(src), src->depth,1 );
	 img_bin_hough=cvCloneImage(src);

	cvCreateTrackbar("threshold3","Hough Calibration", &threshold , 255 , seuillage_hough );
	cvCreateTrackbar("rho","Hough Calibration", &rho , 255 , seuillage_hough );

    cvWaitKey(0);
	printf("chosen thresholds : %ld %ld (thresh rho)\n\n\n",threshold,rho);
	fprintf(fichier,"thresh %ld rho %ld (thresh rho)\n",threshold,rho);
	cvDestroyWindow( "Hough Calibration" );
	cvReleaseImage(&img_hough);
     cvReleaseImage(&img_nvg_hough);
	 cvReleaseImage(&img_bin_hough);

	 cvReleaseImage(&dst);
	 cvReleaseImage(&color_dst);
}


CvPoint Hough_line_extension_MODIF(IplImage* src, int x, int y)
{
	vector<Droite> Array;
	Droite droite;

    IplImage* dst = cvCreateImage( cvGetSize(src), 8, 1 ); 
    IplImage* color_dst = cvCreateImage( cvGetSize(src), 8, 3 ); 
    CvMemStorage* storage = cvCreateMemStorage(0); 
    CvSeq* lines = 0; 
    int i; 

    cvCanny( src, dst, 10,200, 3 ); 
    //cvCvtColor( dst, color_dst, CV_GRAY2BGR );

	lines = cvHoughLines2( dst, storage, CV_HOUGH_PROBABILISTIC, rho, CV_PI/360, threshold, 0, 0 ); 

    for( i = 0; i < MIN(lines->total,100); i++ ) 
    { 
        CvPoint* line = (CvPoint*)cvGetSeqElem(lines,i); 
        CvPoint pt1=line[0];
		CvPoint	pt2=line[1]; 
		if(Dist_segment2(x,y,pt1.x, pt1.y, pt2.x, pt2.y)<1600 &&Dist2(pt1.x, pt1.y, pt2.x, pt2.y)>5)
		{
			//ax+by=c
			//ligne � ralonger (on peut supposer c=1 car pas de biais : verti ou hori
			double d=pt1.x*pt2.y-pt1.y*pt2.x;
			double a=((double)(pt2.y-pt1.y))/d;
			double b=((double)(pt1.x-pt2.x))/d; 
			if(b==0)//m�me abs
			{
				pt1.y=0;
				pt2.y=480;
			}
			else
			{
				pt1.x=0;
				pt1.y=1/b;

				pt2.x=640;
				pt2.y=(1-640*a)/b;
			}

			droite.a=a;
			droite.b=b;

			Array.push_back(droite);

			cvLine( color_dst, pt1, pt2, CV_RGB(0,255,0)); //les lignes ralong�es
		}
		else
		{
			cvLine( color_dst, pt1, pt2, CV_RGB(255,0,0)); 
		}

		
     } 

     cvNamedWindow( "Source", 1 ); 
     cvShowImage( "Source", src ); 

     cvNamedWindow( "Hough", 1 ); 
     cvShowImage( "Hough", color_dst ); 
		
     cvReleaseImage(&dst);
	 cvReleaseImage(&color_dst);

	Droite d1,d2;
	if(Array.empty() || Array.size()==1)
	{
		CvPoint pt;

		pt.x=-1;
		pt.y=-1;//cad pas d'intersection

		return pt;
	}
	if(Array.size()==2)
	{
		d1=Array.back();
		Array.pop_back();

		d2=Array.back();
		Array.pop_back();
	}
	else
	{
		//faire le tri : on veut 2 droites dont l'angle est pas trop petit
		d1=Array.back();
		Array.pop_back();

		d2=Array.back();
		Array.pop_back();
		while(Meme_Angle(d1,d2))
		{
			if(Array.empty())
			{
				CvPoint pt;

				pt.x=-1;
				pt.y=-1;//cad pas d'intersection

				return pt;
			}

			d2=Array.back();
			Array.pop_back();
		}
	}

	return Intersection_droites(d1,d2);
}



/*void Hough_line(IplImage* src)
{

    IplImage* dst = cvCreateImage( cvGetSize(src), 8, 1 ); 
    IplImage* color_dst = cvCreateImage( cvGetSize(src), 8, 3 ); 
    CvMemStorage* storage = cvCreateMemStorage(0); 
    CvSeq* lines = 0; 
    int i; 

    cvCanny( src, dst, 10,200, 3 ); 
    //cvCvtColor( dst, color_dst, CV_GRAY2BGR );

	lines = cvHoughLines2( dst, storage, CV_HOUGH_PROBABILISTIC, rho, CV_PI/360, threshold, 0, 0 ); 

    for( i = 0; i < MIN(lines->total,100); i++ ) 
    { 
        CvPoint* line = (CvPoint*)cvGetSeqElem(lines,i); 
        CvPoint pt1=line[0];
		CvPoint	pt2=line[1]; 

        cvLine( color_dst, pt1, pt2, CV_RGB(255,0,0)); 
     } 

     cvNamedWindow( "Source", 1 ); 
     cvShowImage( "Source", src ); 

     cvNamedWindow( "Hough", 1 ); 
     cvShowImage( "Hough", color_dst ); 
		
     cvReleaseImage(&dst);
	 cvReleaseImage(&color_dst);
}

*/
int bin_val=8;

IplImage* img_dst;
IplImage* img_src;

/*
void seuil_binarisation(int valeur)
{
	cvThreshold( img_src, img_dst, bin_val, 255,CV_THRESH_BINARY);
	cvNamedWindow( "binarization", 1 ); 
	cvShowImage( "binarization", img_dst);
}*/

void Binarisation(IplImage *src,IplImage *dst)
{
	img_dst=cvCreateImage(cvGetSize(dst), dst->depth, 1);
	img_src=cvCreateImage(cvGetSize(src), src->depth, 1);
	img_src=cvCloneImage(src);

	cvThreshold( src, dst, bin_val, 255,CV_THRESH_BINARY);

	cvNamedWindow("binarization", CV_WINDOW_AUTOSIZE);
	cvShowImage("binarization", dst);

	//cvCreateTrackbar("bin_val","binarization", &bin_val, 255,seuil_binarisation);
}

void Ouverture(IplImage *src,IplImage *dst)
{

IplImage *temp=cvCreateImage(cvGetSize(src), src->depth, 1);

	IplConvKernel* noyau = cvCreateStructuringElementEx(9, 9, 4, 4, CV_SHAPE_RECT);
	cvMorphologyEx(src, dst, temp, noyau, CV_MOP_OPEN);

	cvNamedWindow("testtt", CV_WINDOW_AUTOSIZE);
	cvShowImage("testtt", dst);
	cvReleaseImage(&temp);

}




int levels = 3;
CvSeq* contours = 0;

void Parcourt_contours_figure_MODIF(CvSeq* seq,IplImage* dst,int x, int y,CvScalar color,int pos_x, int pos_y, int pos_z, double theta, double phi, double z_postule)
{


	double c_vert=1085.23/1.5;
	double c_hori=1069/1.5;//TEMPORAIRE, en faire un truc global et c_vert et c_hori : pareil


	/*int pos_x=188;
	int pos_y=179;
	int pos_z=310;
	double theta=0;
	double phi=20;
	double z_postule=0;*/

	//cvDrawContours( dst, seq, CV_RGB(255,0,0), CV_RGB(0,255,0), levels, 1, CV_AA);

	Tache tache=calcul_contour(seq,dst,x,y);

	if(tache.surface>=800)//arbitraire mais un peu en dessous de la taille d'un pion super loin
	{
		Coord3D p=calculer_coord_espace(tache.x_centre, tache.y_centre,pos_x, pos_y, pos_z, theta,phi, 0);

		CvPoint b1=calculer_coord_ecran(p.x+80*cos(theta),p.y+80*sin(theta),p.z-20,pos_x,pos_y,pos_z,theta,phi);//base
		CvPoint b2=calculer_coord_ecran(p.x-80*cos(theta),p.y-80*sin(theta),p.z-20,pos_x,pos_y,pos_z,theta,phi);
		CvPoint b3=calculer_coord_ecran(p.x-80*sin(theta),p.y+80*cos(theta),p.z-20,pos_x,pos_y,pos_z,theta,phi);
		CvPoint b4=calculer_coord_ecran(p.x+80*sin(theta),p.y-100*cos(theta),p.z-20,pos_x,pos_y,pos_z,theta,phi);

		CvPoint s1=calculer_coord_ecran(p.x+80*cos(theta),p.y+80*sin(theta),p.z+20,pos_x,pos_y,pos_z,theta,phi);//sommet
		CvPoint s2=calculer_coord_ecran(p.x-80*cos(theta),p.y-80*sin(theta),p.z+20,pos_x,pos_y,pos_z,theta,phi);
		CvPoint s3=calculer_coord_ecran(p.x-80*sin(theta),p.y+80*cos(theta),p.z+20,pos_x,pos_y,pos_z,theta,phi);
		CvPoint s4=calculer_coord_ecran(p.x+80*sin(theta),p.y-80*cos(theta),p.z+20,pos_x,pos_y,pos_z,theta,phi);

		int Max_y=max(max(max(b1.y,b2.y),b3.y),b4.y);
		int min_y=min(min(min(s1.y,s2.y),s3.y),s4.y);
		int Max_x=max(max(max(b1.x,b2.x),b3.x),b4.x);
		int min_x=min(min(min(b1.x,b2.x),b3.x),b4.x);

		if(tache.surface>=(Max_y-min_y)*(Max_x-min_x))//taille approx de la tranche d'un pion
		{
			// surfaces : %ld %ld
			//printf("noeud: %ld %ld pion: %d %d Coord3D: %lf %lf %lf\n", x,y,tache.x_centre, tache.y_centre,p.x,p.y,p.z/*,(Max_y-min_y)*(Max_x-min_x),tache.surface*/);
			
			//trouver la forme de la tache

			int N = tache.contour->total;
			CvSeqReader reader;
			int i;
			CvPoint pt;

			cvStartReadSeq(seq, &reader);

			int x_min=dst->width-1;
			int y_min=dst->height-1;
			int x_max=0;
			int y_max=0;


			for (i = 1; i < N; ++i)
			{
				CV_READ_SEQ_ELEM(pt, reader);

				if(pt.x<x_min)x_min=pt.x;
				if(pt.y<y_min)y_min=pt.y;
				if(pt.x>x_max)x_max=pt.x;
				if(pt.y>y_max)y_max=pt.y;
			}//rectangle dans lequel est le contour

			CvPoint Sommet;//un des points les plus haut
			CvPoint Pied;//un des points les plus bas
			CvPoint Droite_haut;//point qui parmi les plus � droite � 2 ou 3 pixels pret est le plus haut
			CvPoint Gauche_haut;//point qui parmi les plus � gauche � 2 ou 3 pixels pret est le plus haut
			CvPoint Droite_bas;//point qui parmi les plus � droite � 2 ou 3 pixels pret est le plus bas
			CvPoint Gauche_bas;//point qui parmi les plus � gauche � 2 ou 3 pixels pret est le plus bas

			Sommet.x=0;
			Sommet.y=y_min;

			Pied.x=0;
			Pied.y=y_max;

			Droite_haut.x=x_max;
			Droite_haut.y=dst->height-1;

			Gauche_haut.x=x_min;
			Gauche_haut.y=dst->height-1;

			Droite_bas.x=x_max;
			Droite_bas.y=0;

			Gauche_bas.x=x_min;
			Gauche_bas.y=0;

			for (i = 1; i < N; ++i)
			{
				CV_READ_SEQ_ELEM(pt, reader);

				if(pt.y==Sommet.y)Sommet.x=pt.x;
				if(pt.y==Pied.y)Pied.x=pt.x;

				if(Proche(pt.x,Droite_haut.x) && pt.y<Droite_haut.y)Droite_haut.y=pt.y;
				if(Proche(pt.x,Droite_bas.x) && pt.y>Droite_bas.y)Droite_bas.y=pt.y;

				if(Proche(pt.x,Gauche_haut.x) && pt.y<Gauche_haut.y)Gauche_haut.y=pt.y;
				if(Proche(pt.x,Gauche_bas.x) && pt.y>Gauche_bas.y)Gauche_bas.y=pt.y;
			}

			//faire des trucs pour trouver la forme du contour
			if( (AssezProche(Sommet.x,Droite_haut.x) && AssezProche(Sommet.y,Droite_haut.y) && AssezProche(Droite_haut.y,Gauche_haut.y))
			  ||(AssezProche(Sommet.x,Gauche_haut.x) && AssezProche(Sommet.y,Gauche_haut.y)&& AssezProche(Gauche_haut.y,Droite_haut.y))  )
			{
				//tranche
				//printf("tranche: %ld %ld\n",Sommet.x,Sommet.y);

				printf("slice %ld %ld\n",	tache.x_centre,	tache.y_centre);
				fprintf(fichier,"tranche: %ld %ld\n",	tache.x_centre,	tache.y_centre);
				Coord3D temp=calculer_coord_espace(tache.x_centre, tache.y_centre,pos_x,pos_y,pos_z,theta,phi,z_postule);
				printf("  3D coordinates: %lf %lf %lf\n\n\n",	temp.x, temp.y, temp.z);
				fprintf(fichier, "Coord 3D %lf %lf %lf\n",	temp.x, temp.y, temp.z);

				AfficherContour(dst,seq,CV_RGB(255,0,0));
				cvDrawContours( dst,seq, CV_RGB(255,0,0), CV_RGB(255,0,0), 0, 1, CV_AA);
			}
			else if(AssezProche(Droite_haut.y,Droite_bas.y) && AssezProche(Gauche_haut.y,Gauche_bas.y))
			{
				//sommet du pion

				/*Recherche_Coord_Pion(cvPoint(tache.x_centre,tache.y_centre), Gauche_bas,pos_x,pos_y, pos_z, theta, phi, c_hori, c_vert);
				Recherche_Coord_Pion(cvPoint(tache.x_centre,tache.y_centre), Gauche_haut,pos_x,pos_y, pos_z, theta, phi, c_hori, c_vert);
				Recherche_Coord_Pion(cvPoint(tache.x_centre,tache.y_centre), Droite_bas,pos_x,pos_y, pos_z, theta, phi, c_hori, c_vert);
				Recherche_Coord_Pion(cvPoint(tache.x_centre,tache.y_centre), Gauche_haut,pos_x,pos_y, pos_z, theta, phi, c_hori, c_vert);*/

				AfficherContour(dst,seq,CV_RGB(0,255,0));
				cvDrawContours( dst,seq, CV_RGB(0,255,0), CV_RGB(0,255,0), 0, 1, CV_AA);
			}
			else//tester la forme du pion
			{
				//pion entier
				//printf("pion: %ld %ld\n",Sommet.x,Sommet.y);
				AfficherContour(dst,seq,CV_RGB(0,0,255));
				cvDrawContours( dst,seq, CV_RGB(0,0,255), CV_RGB(0,0,128), 0, 1, CV_AA);
			}
		}
	}

	if(seq->h_next!=NULL)
	{
		Parcourt_contours_figure_MODIF(seq->h_next,dst,x+1,y,CV_RGB(255,0,0),pos_x,pos_y,pos_z,theta,phi,z_postule);
	}

	if(seq->v_next!=NULL)
	{
		Parcourt_contours_figure_MODIF(seq->v_next,dst,x,y+1,CV_RGB(0,255,0),pos_x,pos_y,pos_z,theta,phi,z_postule);
	}

}

void Parcourt_contours_MODIF(CvSeq* seq,IplImage* dst,int x, int y,CvScalar color,int pos_x, int pos_y, int pos_z, double theta, double phi, double z_postule)
{


	double c_vert=1085.23/1.5;
	double c_hori=1069/1.5;//TEMPORAIRE, en faire un truc global et c_vert et c_hori : pareil


	/*int pos_x=188;
	int pos_y=179;
	int pos_z=310;
	double theta=0;
	double phi=20;
	double z_postule=0;*/

	//cvDrawContours( dst, seq, CV_RGB(255,0,0), CV_RGB(0,255,0), levels, 1, CV_AA);

	Tache tache=calcul_contour(seq,dst,x,y);

	if(tache.surface>=800)//arbitraire mais un peu en dessous de la taille d'un pion super loin
	{
		Coord3D p=calculer_coord_espace(tache.x_centre, tache.y_centre,pos_x, pos_y, pos_z, theta,phi, 0);

		CvPoint b1=calculer_coord_ecran(p.x+80*cos(theta),p.y+80*sin(theta),p.z-20,pos_x,pos_y,pos_z,theta,phi);//base
		CvPoint b2=calculer_coord_ecran(p.x-80*cos(theta),p.y-80*sin(theta),p.z-20,pos_x,pos_y,pos_z,theta,phi);
		CvPoint b3=calculer_coord_ecran(p.x-80*sin(theta),p.y+80*cos(theta),p.z-20,pos_x,pos_y,pos_z,theta,phi);
		CvPoint b4=calculer_coord_ecran(p.x+80*sin(theta),p.y-100*cos(theta),p.z-20,pos_x,pos_y,pos_z,theta,phi);

		CvPoint s1=calculer_coord_ecran(p.x+80*cos(theta),p.y+80*sin(theta),p.z+20,pos_x,pos_y,pos_z,theta,phi);//sommet
		CvPoint s2=calculer_coord_ecran(p.x-80*cos(theta),p.y-80*sin(theta),p.z+20,pos_x,pos_y,pos_z,theta,phi);
		CvPoint s3=calculer_coord_ecran(p.x-80*sin(theta),p.y+80*cos(theta),p.z+20,pos_x,pos_y,pos_z,theta,phi);
		CvPoint s4=calculer_coord_ecran(p.x+80*sin(theta),p.y-80*cos(theta),p.z+20,pos_x,pos_y,pos_z,theta,phi);

		int Max_y=max(max(max(b1.y,b2.y),b3.y),b4.y);
		int min_y=min(min(min(s1.y,s2.y),s3.y),s4.y);
		int Max_x=max(max(max(b1.x,b2.x),b3.x),b4.x);
		int min_x=min(min(min(b1.x,b2.x),b3.x),b4.x);

		if(tache.surface>=(Max_y-min_y)*(Max_x-min_x))//taille approx de la tranche d'un pion
		{
			// surfaces : %ld %ld
			//printf("noeud: %ld %ld pion: %d %d Coord3D: %lf %lf %lf\n", x,y,tache.x_centre, tache.y_centre,p.x,p.y,p.z/*,(Max_y-min_y)*(Max_x-min_x),tache.surface*/);
			
			//trouver la forme de la tache

			int N = tache.contour->total;
			CvSeqReader reader;
			int i;
			CvPoint pt;

			cvStartReadSeq(seq, &reader);

			int x_min=dst->width-1;
			int y_min=dst->height-1;
			int x_max=0;
			int y_max=0;


			for (i = 1; i < N; ++i)
			{
				CV_READ_SEQ_ELEM(pt, reader);

				if(pt.x<x_min)x_min=pt.x;
				if(pt.y<y_min)y_min=pt.y;
				if(pt.x>x_max)x_max=pt.x;
				if(pt.y>y_max)y_max=pt.y;
			}//rectangle dans lequel est le contour

			CvPoint Sommet;//un des points les plus haut
			CvPoint Pied;//un des points les plus bas
			CvPoint Droite_haut;//point qui parmi les plus � droite � 2 ou 3 pixels pret est le plus haut
			CvPoint Gauche_haut;//point qui parmi les plus � gauche � 2 ou 3 pixels pret est le plus haut
			CvPoint Droite_bas;//point qui parmi les plus � droite � 2 ou 3 pixels pret est le plus bas
			CvPoint Gauche_bas;//point qui parmi les plus � gauche � 2 ou 3 pixels pret est le plus bas

			Sommet.x=0;
			Sommet.y=y_min;

			Pied.x=0;
			Pied.y=y_max;

			Droite_haut.x=x_max;
			Droite_haut.y=dst->height-1;

			Gauche_haut.x=x_min;
			Gauche_haut.y=dst->height-1;

			Droite_bas.x=x_max;
			Droite_bas.y=0;

			Gauche_bas.x=x_min;
			Gauche_bas.y=0;

			for (i = 1; i < N; ++i)
			{
				CV_READ_SEQ_ELEM(pt, reader);

				if(pt.y==Sommet.y)Sommet.x=pt.x;
				if(pt.y==Pied.y)Pied.x=pt.x;

				if(Proche(pt.x,Droite_haut.x) && pt.y<Droite_haut.y)Droite_haut.y=pt.y;
				if(Proche(pt.x,Droite_bas.x) && pt.y>Droite_bas.y)Droite_bas.y=pt.y;

				if(Proche(pt.x,Gauche_haut.x) && pt.y<Gauche_haut.y)Gauche_haut.y=pt.y;
				if(Proche(pt.x,Gauche_bas.x) && pt.y>Gauche_bas.y)Gauche_bas.y=pt.y;
			}

			//faire des trucs pour trouver la forme du contour
			if( (AssezProche(Sommet.x,Droite_haut.x) && AssezProche(Sommet.y,Droite_haut.y))
			  ||(AssezProche(Sommet.x,Gauche_haut.x) && AssezProche(Sommet.y,Gauche_haut.y))  )
			{
				//tranche
				//printf("tranche: %ld %ld\n",Sommet.x,Sommet.y);
				AfficherContour(dst,seq,CV_RGB(255,0,0));
				cvDrawContours( dst,seq, CV_RGB(255,0,0), CV_RGB(255,0,0), 0, 1, CV_AA);
			}
			else if(AssezProche(Droite_haut.y,Droite_bas.y) && AssezProche(Gauche_haut.y,Gauche_bas.y))
			{
				//sommet du pion
				printf("top %ld %ld\n",	tache.x_centre,	tache.y_centre);
				fprintf(fichier,"sommet: %ld %ld\n",	tache.x_centre,	tache.y_centre);
				Coord3D temp=calculer_coord_espace(tache.x_centre, tache.y_centre,pos_x,pos_y,pos_z,theta,phi,z_postule);
				printf("  3D coordinates: %lf %lf %lf\n\n\n",	temp.x, temp.y, temp.z);
				fprintf(fichier, "Coord 3D %lf %lf %lf\n",	temp.x, temp.y, temp.z);

				/*Recherche_Coord_Pion(cvPoint(tache.x_centre,tache.y_centre), Gauche_bas,pos_x,pos_y, pos_z, theta, phi, c_hori, c_vert);
				Recherche_Coord_Pion(cvPoint(tache.x_centre,tache.y_centre), Gauche_haut,pos_x,pos_y, pos_z, theta, phi, c_hori, c_vert);
				Recherche_Coord_Pion(cvPoint(tache.x_centre,tache.y_centre), Droite_bas,pos_x,pos_y, pos_z, theta, phi, c_hori, c_vert);
				Recherche_Coord_Pion(cvPoint(tache.x_centre,tache.y_centre), Gauche_haut,pos_x,pos_y, pos_z, theta, phi, c_hori, c_vert);*/

				AfficherContour(dst,seq,CV_RGB(0,255,0));
				cvDrawContours( dst,seq, CV_RGB(0,255,0), CV_RGB(0,255,0), 0, 1, CV_AA);
			}
			else//tester la forme du pion
			{
				//pion entier
				//printf("pion: %ld %ld\n",Sommet.x,Sommet.y);
				AfficherContour(dst,seq,CV_RGB(0,0,255));
				cvDrawContours( dst,seq, CV_RGB(0,0,255), CV_RGB(0,0,128), 0, 1, CV_AA);
			}
		}
	}

	if(seq->h_next!=NULL)
	{
		Parcourt_contours_MODIF(seq->h_next,dst,x+1,y,CV_RGB(255,0,0),pos_x,pos_y,pos_z,theta,phi,z_postule);
	}

	if(seq->v_next!=NULL)
	{
		Parcourt_contours_MODIF(seq->v_next,dst,x,y+1,CV_RGB(0,255,0),pos_x,pos_y,pos_z,theta,phi,z_postule);
	}

}
/*
void on_trackbar(int pos)
{
	int pos_x=188;
	int pos_y=179;
	int pos_z=380;
	double theta=0;
	double phi=10;
	double z_postule=0;

    IplImage* cnt_img = cvCreateImage( cvSize(640,480), 8, 3 );
    CvSeq* _contours = contours;
    int _levels = levels - 3;
    if( _levels <= 0 ) // get to the nearest face to make it look more funny
        _contours = _contours->h_next->h_next->h_next;
    cvZero( cnt_img );
    cvDrawContours( cnt_img, _contours, CV_RGB(255,0,0), CV_RGB(0,255,0), _levels, 3, CV_AA);
	Parcourt_contours_MODIF(contours,cnt_img,1,1,CV_RGB(255,0,0),pos_x,pos_y,pos_z,theta,phi,z_postule);
    cvShowImage( "Borders", cnt_img );
    cvReleaseImage( &cnt_img );	   
}
*/
void test_contours_MODIF(IplImage* src,int pos_x, int pos_y, int pos_z, double phi)
{
    int i, j;
    CvMemStorage* storage = cvCreateMemStorage(0);

	IplImage *img= cvCreateImage(cvGetSize(src), IPL_DEPTH_8U, 1 );

		//cvCvtColor(src,img,CV_RGB2GRAY); si 3 couleurs
		cvCopyImage(src,img);

    cvNamedWindow( "image", 1 );
    cvShowImage( "image", img );

    cvFindContours( img, storage, &contours, sizeof(CvContour),
                    CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cvPoint(0,0) );

    contours = cvApproxPoly( contours, sizeof(CvContour), storage, CV_POLY_APPROX_DP, 1, 1 );

	double theta=0;
	double z_postule=50;

	IplImage* cnt_img = cvCreateImage( cvSize(640,480), 8, 3 );
	cvZero( cnt_img );
	Parcourt_contours_MODIF(contours,cnt_img,1,1,CV_RGB(255,0,0),pos_x,pos_y,pos_z,theta,phi,z_postule);
	cvShowImage( "Borders", cnt_img );
	printf("If the top of each paw is not displayed as a green circle, it is not good!\n");
	//printf("Si les sommets des pions ne sont pas affiches comme des cercles vert, c'est pas bon!\n");
	cvDestroyWindow( "Paw opening (intermediate)" );
	cvDestroyWindow( "Paw dilation (result)" );
	cvDestroyWindow( "Hough" );
	cvDestroyWindow( "Source" );
	cvDestroyWindow( "image" );
	cvDestroyWindow( "Original" );

    /*cvCreateTrackbar( "levels+3", "Borders", &levels, 7, on_trackbar );
    
    on_trackbar(0);*/
    cvWaitKey(0);
	cvReleaseImage( &cnt_img );	 
    cvReleaseMemStorage( &storage );
	cvReleaseImage(&img);
}


void test_contours_figure_MODIF(IplImage* src,int pos_x, int pos_y, int pos_z, double phi)
{
    int i, j;
    CvMemStorage* storage = cvCreateMemStorage(0);

	IplImage *img= cvCreateImage(cvGetSize(src), IPL_DEPTH_8U, 1 );

		//cvCvtColor(src,img,CV_RGB2GRAY); si 3 couleurs
		cvCopyImage(src,img);

    cvNamedWindow( "image", 1 );
    cvShowImage( "image", img );

    cvFindContours( img, storage, &contours, sizeof(CvContour),
                    CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cvPoint(0,0) );

    contours = cvApproxPoly( contours, sizeof(CvContour), storage, CV_POLY_APPROX_DP, 1, 1 );

	double theta=0;
	double z_postule=50;

	IplImage* cnt_img = cvCreateImage( cvSize(640,480), 8, 3 );
	cvZero( cnt_img );
	Parcourt_contours_figure_MODIF(contours,cnt_img,1,1,CV_RGB(255,0,0),pos_x,pos_y,pos_z,theta,phi,z_postule);
	cvShowImage( "Borders", cnt_img );

	//printf("Si les tranches des figures ne sont pas affiches comme des formes rouges, c'est pas bon!\n");
	printf("If the border of the figures are not displayed as red shapes, it is not good!\n");

	cvDestroyWindow( "Paw opening (intermediate)" );
	cvDestroyWindow( "Paw dilation (result)" );
	cvDestroyWindow( "Hough" );
	cvDestroyWindow( "Source" );
	cvDestroyWindow( "image" );
	cvDestroyWindow( "Original" );

    /*cvCreateTrackbar( "levels+3", "Borders", &levels, 7, on_trackbar );
    
    on_trackbar(0);*/
    cvWaitKey(0);
	cvReleaseImage( &cnt_img );	 
    cvReleaseMemStorage( &storage );
	cvReleaseImage(&img);
}


Coord3D trouver_repere_MODIF(double pos_z, double phi, IplImage* src, bool &est_rouge)
{
//on choisit le point du quadrillage le plus proche le long de la bordure noir

	double x,y,z;

CvPoint repere_couleur=calculer_coord_ecran(190, 170, pos_z,100, 1000, 0, 0, phi);
// point dans la case juste devant le robot

		//ordre (x,y) invers� ATTENTION
	double val=cvGet2D(src,repere_couleur.y,repere_couleur.x).val[0]; 

	est_rouge=(val>100); //vrai si la case est rouge donc on est rouge : ATTENTION DEPEND LA COULEUR DE LA CASE DEPEND DE L'orientation (modifier le test...) de la cam (phi=10)

	if(est_rouge)
	{
		//le point de repere est (0,800,0) pour 20� : DEPEND DE L INCLINAISON DE LA CAM
		x=0;
		y=1150;
		z=0;
	}
	else
	{
		//le point de repere est (0,2200,0) pour 20� : DEPEND DE L INCLINAISON DE LA CAM
		x=0;
		y=1870;
		z=0;
	}
	
	//coord tr�s grossiers
	CvPoint repere_approx=calculer_coord_ecran(190, 170, pos_z,x, y, z, 0, phi);

	CvPoint intersection=Hough_line_extension_MODIF(src,repere_approx.x,repere_approx.y);//(115,325) position approximative du point pris comme rep�re. Le r�sultat est la position pr�cise
	printf("The intersection found is : %ld %ld\n", intersection.x, intersection.y);
	//printf("l'intersection trouvee est : %ld %ld\n",intersection.x,intersection.y);
	fprintf(fichier,"intersection %ld %ld\n",intersection.x,intersection.y);
	//(x,y,z) position de l'objet dont qu'on utilise comme rep�re, (x_ecran, y_ecran) sa position dans l'image
	Coord3D depart=calculer_position_initiale(est_rouge, x, y, z, intersection.x, intersection.y, pos_z, phi);
	printf("The starting point is %lf %lf %lf\n\n\n", depart.x, depart.y, depart.z);
	//printf("Grace au repere,\n on trouve que le Point de depart est %lf %lf %lf\n\n\n",depart.x,depart.y,depart.z);
	fprintf(fichier,"Point de depart %lf %lf %lf\n\n",depart.x,depart.y,depart.z);
	
	return depart;
}

int main()
{
    fichier = fopen("sortie.txt", "w");

	IplImage *img;
	IplImage *img_nvg;
	IplImage *img_cont; 
	IplImage *img_test;
	IplImage *img_test2;
	IplImage *img_test3;
	IplImage *temp; 

	IplImage *red; 
	IplImage *green; 
	IplImage *blue; 

    //Initialisations
    img=cvLoadImage("test.jpg");

	if (img == NULL)
	{
		printf("test.jpg absent!\n");
		return -1;
	}

	img_nvg=cvCreateImage(cvGetSize(img), img->depth, 1);
    img_cont=cvCloneImage(img_nvg);
    temp=cvCloneImage(img_nvg);

    //Contr�le de l'origine
    int flip=0;
    if(img->origin!=IPL_ORIGIN_TL){
        flip=CV_CVTIMG_FLIP;
    }
    cvConvertImage(img, img_nvg, flip);

    cvNamedWindow("Original", CV_WINDOW_AUTOSIZE);

   cvShowImage("Original", img);

	red=cvCreateImage( cvGetSize(img), img->depth,1 );
	green=cvCreateImage( cvGetSize(img), img->depth,1 );
	blue=cvCreateImage( cvGetSize(img), img->depth,1 );
	cvSplit(img,blue,green,red,NULL);

    IplConvKernel* element = cvCreateStructuringElementEx(3,3,1,1,CV_SHAPE_CROSS);
cvMorphologyEx(img_nvg, img_cont,temp,element,CV_MOP_GRADIENT);

	img_test=cvCreateImage( cvGetSize(img), img->depth,1 );
	img_test2=cvCreateImage( cvGetSize(img), img->depth,1 );
	img_test3=cvCreateImage( cvGetSize(img), img->depth,1 );

	etalonnage_rouge(red, green, blue,img_test);

	Hough_line_etalonnage(img_test);

	bool est_rouge;//trouver_repere va donner notre couleur (m�me si la carte RGB le fait aussi...)
	Coord3D ma_position_initiale=trouver_repere_MODIF(380, 12.6,img_test, est_rouge);

	img_pion1=cvCloneImage(img);

	etalonnage_blanc(img,img_test3);
	IplImage* temp6=cvCreateImage( cvGetSize(img_test3), img_test3->depth,1 );
	cvCopy(temp3,temp6);

	etalonnage_jaune(img,img_test3);//liberer les trucs � la fin
	cvNamedWindow("Yellow component", CV_WINDOW_AUTOSIZE);
	cvShowImage("Yellow component",temp3);

IplImage* temp1=cvCreateImage( cvGetSize(img_test3), img_test3->depth,1 );
IplImage* temp2=cvCreateImage( cvGetSize(img_test3), img_test3->depth,1 );
IplImage* temp4=cvCreateImage( cvGetSize(img_test3), img_test3->depth,1 );
IplImage* temp5=cvCreateImage( cvGetSize(img_test3), img_test3->depth,1 );

IplConvKernel* noyau = cvCreateStructuringElementEx(5, 5, 2, 2, CV_SHAPE_RECT);
cvMorphologyEx(temp3, temp2, temp1, noyau, CV_MOP_OPEN);

cvNamedWindow( "Paw opening (intermediate)", 1 ); 
cvShowImage( "Paw opening (intermediate)", temp2);

IplConvKernel* noyau2 = cvCreateStructuringElementEx(5, 5, 2, 2, CV_SHAPE_RECT);
cvDilate(temp2, temp3, noyau2);

cvNamedWindow( "Paw dilation (result)", 1 ); 
cvShowImage( "Paw dilation (result)", temp3);

cvAnd(img_cont,temp3,temp4);

cvNamedWindow( "Paw borders", 1 ); 
cvShowImage( "Paw borders", temp4);

	Binarisation(temp4,temp5);

test_contours_MODIF(temp5,ma_position_initiale.x,ma_position_initiale.y,ma_position_initiale.z,12.6);


cvMorphologyEx(temp6, temp2, temp1, noyau, CV_MOP_OPEN);

cvNamedWindow( "Figure opening (intermediate)", 1 ); 
cvShowImage( "Figure opening (intermediate)", temp2);

cvDilate(temp2, temp3, noyau2);

cvNamedWindow( "Figure dilation (result)", 1 ); 
cvShowImage( "Figure dilation (result)", temp3);

cvAnd(img_cont,temp3,temp4);

cvNamedWindow( "figure_contours", 1 ); 
cvShowImage( "figure_contours", temp4);

Binarisation(temp4,temp5);

test_contours_figure_MODIF(temp5,ma_position_initiale.x,ma_position_initiale.y,ma_position_initiale.z,12.6);

cvReleaseImage(&temp1);
cvReleaseImage(&temp2);
cvReleaseImage(&temp3);
cvReleaseImage(&temp4);
cvReleaseImage(&temp5);
cvReleaseImage(&temp6);
   
cvWaitKey(0);

cvReleaseImage(&img);
cvReleaseImage(&img_nvg);
cvReleaseImage(&img_cont);
cvReleaseImage(&temp);
cvReleaseImage(&img_test);
cvReleaseImage(&img_test2);
cvReleaseImage(&img_test3);
cvReleaseImage(&img_src);
cvReleaseImage(&img_dst);

fclose(fichier);

return 0;
}




