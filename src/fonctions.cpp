#include "fonctions.h" 

CvPoint calculer_coord_ecran(double x, double y, double z,int pos_x, int pos_y, int pos_z, double theta, double phi)
{
	//z vertical vers le haut, y dans la profondeur, coin case de depart robot rouge :origine

	theta=theta*2*3.141592653589/360;
	phi=phi*2*3.141592653589/360;

	double c_vert=1085.23/1.5;
	double c_hori=1069/1.5;

	double x2=x-pos_x;
	double y2=y-pos_y;
	double z2=z-pos_z;

	double x3=x2*cos(theta)-y2*sin(theta);
	double y3=x2*sin(theta)+y2*cos(theta);
	double z3=z2;

	double x4=x3;
	double y4=y3*cos(phi)-z3*sin(phi);
	double z4=y3*sin(phi)+z3*cos(phi);

	double y5=-c_vert*z4/y4+480/2;//y2!=0 sinon le point est sur la cam : impossible
	double x5=c_hori*x4/y4+640/2;

	CvPoint c;
	c.x=x5;
	c.y=y5;

	return c;
}



//(x,y,z) position de l'objet dont qu'on utilise comme rep�re, (x_ecran, y_ecran) sa position dans l'image
Coord3D calculer_position_initiale(bool est_rouge, double x, double y, double z, double x_ecran, double y_ecran, double pos_z, double phi)
{
	double ct;

	double c_vert=1085.23/1.5;
	double c_hori=1069/1.5;

	phi=phi*2*3.141592653589/360;

	if(est_rouge)
	{
		ct=1;
		//cos_theta=1;
		//sin_theta=0;
	}
	else
	{
		ct=-1;
		//cos_theta=-1;
		//sin_theta=0;
	}

	double x4,y4,z4;

	//pos_z est connu

	
	
	//il n'y a pas de probl�me parce que le 0 est en dehors de l'�cran pour le denominateur
	
	y4 = (z-pos_z)/ (cos(phi)/c_vert*(480/2-y_ecran)-sin(phi));
	x4 = (x_ecran-640/2)*y4/c_hori;
	z4 =(480/2-y_ecran)*y4/c_vert;
	
	double pos_x = x-ct*x4;
	double pos_y = y-ct * (cos(phi)*y4+sin(phi)*z4);
	
	
	//
	////pos_z-z=y4*sin(phi)-z4*cos(phi)  (1)
	////(x_ecran-640/2)/c_hori=x4/y4     (2)
	////(y_ecran-480/2)/(-c_vert)=z4/y4       (3)
	//if(y_ecran!=480/2)
	//{
		//double v=sin(phi)-cos(phi)*(y_ecran-480/2)/(-c_vert);
			//if(v!=0)//c'est le cas si l'objet observ� a une cote diff de la cam
			//{
				////(3) => (y_ecran-480/2)/(-c_vert)*y4=z4
				////(1) => pos_z-z=y4*(sin(phi)-(y_ecran-480/2)/(-c_vert)*cos(phi))
				////		pos_z-z=y4*v
				//y4=(pos_z-z)/v;

				////(y_ecran-480/2)/(-c_vert)*y4=z4
				//z4=(y_ecran-480/2)/(-c_vert)*y4;
				////y4*(x_ecran-640/2)/c_hori=x4
				//x4=y4*(x_ecran-640/2)/(c_hori);
			//}
	//}
	//else
	//{
	//	z4=0;
		//y4=(pos_z-z)/sin(phi);
		//x4=y4*(x_ecran-640/2)/c_hori;
	//}

	//double pos_x=x-ct*x4;
	//double pos_y=y-ct*(y4*cos(phi)+z4*sin(phi));

	Coord3D c;
	c.x=pos_x;
	c.y=pos_y;
	c.z=pos_z;

	return c;
}




Coord3D calculer_coord_espace(double x_ecran, double y_ecran,int pos_x, int pos_y, int pos_z, double theta, double phi, double z_postule)
{
	//z vertical vers le haut, y dans la profondeur, coin case de depart robot rouge :origine

	//on postule que z vaut z_postule
	//et on retrouve x et y

	theta=theta*2*3.141592653589/360;
	phi=phi*2*3.141592653589/360;

	double c_vert=1085.23/1.5;
	double c_hori=1069/1.5;

	double x4,y4,z4;
	double x3,y3,z3;
	double x2,y2,z2;
	double x,y,z;

	//on a z_postule=z2+pox_z et z2=z3 et z3=-y4*sin(phi)+z4*cos(phi)
	//donc z_postule=-y4*sin(phi)+z4*cos(phi)+pox_z
	//donc y4*sin(phi)=z4*cos(phi)+pox_z-z_postule

	if(sin(phi)!=0)
	{
		//donc y4=(z4*cos(phi)+pox_z-z_postule)/sin(phi)

		if((y_ecran-480/2)*cos(phi)+c_vert*sin(phi) !=0 )
		{
			//donc
			z4=(y_ecran-480/2)*(z_postule-pos_z)/((y_ecran-480/2)*cos(phi)+c_vert*sin(phi));
		}
		else
		{
			//on approxime par 0.1
			z4=(y_ecran-480/2)*(z_postule-pos_z)/0.1;
		}
		y4=(z4*cos(phi)+pos_z-z_postule)/sin(phi);
		x4=(x_ecran-640/2)*y4/c_hori;
	}
	else
	{
		//z4=z_postule-pox_z
		//sin(phi)=0 et cos(phi)=1

		if(y_ecran!=480/2)
		{
			z4=z_postule-pos_z;
			y4=-c_vert*z4/(y_ecran-480/2);
			x4=y4*(x_ecran-640/2)/c_hori;
		}
		else
		{
			//impossible normalement car
			//y_ecran=-c_vert*z4/y4+480/2
			//donc z4=0
			//donc y_ecran=480/2 : au milieu de l'ecran verticalement
			//donc en fait, on une droite de possibilit�, dont un seul point est dans le champ: la camera elle m�me
			z4=0;
		}
	}

	x3=x4;
	y3=y4*cos(phi)+z4*sin(phi);
	z3=-y4*sin(phi)+z4*cos(phi);

	x2=x3*cos(theta)+y3*sin(theta);
	y2=-x3*sin(theta)+y3*cos(theta);
	z2=z3;

	x=x2+pos_x;
	y=y2+pos_y;
	z=z2+pos_z;

	Coord3D c;
	c.x=x;
	c.y=y;
	c.z=z;

	return c;
}



void afficher(CvArr* img, int pos_x, int pos_y, int pos_z, double theta, double phi,Coord3D tab[7][6])
{
	CvPoint tab2[7][6];

	int i,j;

	for(i=0;i<6;i++)
	{
		for(j=0;j<7;j++)
		{
			tab2[j][i]=calculer_coord_ecran(tab[j][i].x, tab[j][i].y, tab[j][i].z,pos_x,pos_y,pos_z, theta, phi);
		}
	}

	for(i=0;i<6;i++)
	{
		cvLine(img, tab2[0][i], tab2[6][i], cvScalar(120));
	}

	for(j=0;j<7;j++)
	{
		cvLine(img, tab2[j][0], tab2[j][5], cvScalar(120));
	}

}








bool pointIsInsideContour(CvSeq *contour, int x, int y)
{
	//We know that a point is inside a contour if we can find one point in the contour that is immediately to the left,
	//one that is immediately on top, one that is immediately to the right and one that is immediately below the (x,y) point.
	//We will update the boolean variables below when these are found:
	char found_left=0, found_top=0, found_right=0, found_bottom=0;
	int count, i; //Variables used for iteration
	CvPoint *contourPoint; //A pointer to a single contour poit.
	
	if(!contour)return 0; //Don't bother doing anything if there is no contour.
	
	count = contour->total; //The total field holds the number of points in the contour.
	
	for(i=0;i<count;i++){ //So, for every point in the contour...
		//We retrieve a pointer to the point at index i using the useful macro CV_GET_SEQ_ELEM
		contourPoint = (CvPoint *)CV_GET_SEQ_ELEM(CvPoint,contour,i);
		
		if(contourPoint->x == x){ //If the point is on the same vertical plane as (x,y)...
			if(contourPoint->y < y)found_top = 1; //and is above (x,y), we found the top.
			else found_bottom = 1; //Otherwise, it's the bottom.
		}
		if(contourPoint->y == y){ //Do the same thing for the horizontal axis...
			if(contourPoint->x < x)found_left = 1;
			else found_right = 1;
		}
	}
	
	return found_left && found_top && found_right && found_bottom; //Did we find all four points?
}



Tache calcul_contour(CvSeq* contour,IplImage* dst, int x, int y)
{
	int N = contour->total;
	CvSeqReader reader;
	int i;
	CvPoint pt;

	cvStartReadSeq(contour, &reader);

	int x_min=dst->width-1;
	int y_min=dst->height-1;
	int x_max=0;
	int y_max=0;


	for (i = 1; i < N; ++i)
	{
		CV_READ_SEQ_ELEM(pt, reader);

		if(pt.x<x_min)x_min=pt.x;
		if(pt.y<y_min)y_min=pt.y;
		if(pt.x>x_max)x_max=pt.x;
		if(pt.y>y_max)y_max=pt.y;
	}//rectangle dans lequel est le contour

	int j,k;

	int nbrePixel=0;//nbre de points dans le contour
	int Somme_x=0;
	int Somme_y=0;
	
	Tache tache;
	tache.surface=(x_max-x_min)*(y_max-y_min);
	tache.contour=contour;

	tache.x_centre=(x_max+x_min)/2;
	tache.y_centre=(y_max+y_min)/2;

	tache.x_min=x_min;
	tache.y_min=y_min;
	tache.x_max=x_max;
	tache.y_max=y_max;

	return tache;
}

bool AssezProche(int a, int b)
{
	return abs(a-b)<20;
}

bool Proche(int a, int b)
{
	return abs(a-b)<3;
}

void AfficherContour(IplImage* dst,CvSeq* seq,CvScalar color)
{
	CvPoint pt;
	int N = seq->total;
	CvSeqReader reader;
	cvStartReadSeq(seq, &reader);

	for (int i = 1; i < N; ++i)
	{
		CV_READ_SEQ_ELEM(pt, reader);
		cvSet2D(dst,pt.y,pt.x,color);
	}
}



void Recherche_Coord_Pion(CvPoint A, CvPoint B,int pos_x, int pos_y, int pos_z, double theta, double phi, double c_hori, double c_vert)
{

double x1=(A.x-640/2)*c_vert;
				double y1=c_hori*c_vert;
				double z1=-(A.y-480/2)*c_hori;

				double n=sqrt(x1*x1+y1*y1+z1*z1);

				x1/=n;
				y1/=n;
				z1/=n;


				double x2=(B.x-640/2)*c_vert;
				double y2=c_hori*c_vert;
				double z2=-(B.y-480/2)*c_hori;

				n=sqrt(x2*x2+y2*y2+z2*z2);

				x2/=n;
				y2/=n;
				z2/=n;

				//on a 2 droites (robot (0,0,0) )+t*(x1, y1, z1)
				//la distance entre deux points � m�me dist de la cam est:
				//sqrt((x1-x2)�+...)*t

				double r_pion=100;

				double t=r_pion/sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)+(z1-z2)*(z1-z2));
				//d'o� les coordonn�es dans l'espace (ne pas oublier le changement de rep�re)


				theta*=2*3.141592653589/360;
				phi*=2*3.141592653589/360;

				x1*=t;
				y1*=t;
				z1*=t;

				x2=x1;
				y2=(y1*cos(phi)+z1*sin(phi));
				z2=(-y1*sin(phi)+z1*cos(phi));

				double x3=x2*cos(theta)+y2*sin(theta);
				double y3=-x2*sin(theta)+y2*cos(theta);
				double z3=z2;

				x2=x3+pos_x;
				y2=y3+pos_y;
				z2=z3+pos_z;



				printf("pion : %lf %lf %lf\n", x2,y2,z2);

}

//distance de (x,y) au segment ( (x1,y1), (x2,y2) )
double Dist2(double x1, double y1, double x2, double y2)
{
	return (x1-x2)*(x1-x2)+(y1-y2)*(y1-y2);
}

//distance � la droite ax+by+c=0
double Dist_droite2(double x, double y,double a, double b, double c)
{
	double d = a*x+b*y+c;
	return (d*d)/(a*a+b*b);
}

double Dist_segment2(double x, double y,double x1, double y1, double x2, double y2)
{
double a=(x-x1)*(x2-x1)+(y-y1)*(y2-y1);

	if(a<0 || a>(y2-y1)*(y2-y1)+(x2-x1)*(x2-x1))//(x,y) se projette en dehors du segment
	{
		double d1 = Dist2(x,y,x1,y1);//distance� au premier point
		double d2 = Dist2(x,y,x2,y2);//distance� au 2eme point
		return min(d1,d2);
	}
	else
	{
		return Dist_droite2(x,y,y1-y2,x2-x1,y2*x1-y1*x2);//distance� � la droite
	}
}

CvPoint Intersection_droites(Droite d1, Droite d2)
{
	CvPoint pt;

	double d=d1.a*d2.b-d1.b*d2.a;

	if(d==0)
	{	
		pt.x=-1;
		pt.y=-1;//cad pas d'intersection
	}
	else
	{
		pt.x=(d2.b-d1.b)/d;
		pt.y=(d1.a-d2.a)/d;
	}
	return pt;
}

double Angle(Droite d)
{
	if(d.b==0)
	{
		return 3.1415/2;
	}
	else
	{
		double angle=atan(d.a/d.b);
		if(angle<0)
		{
			angle+=3.1415;
		}
		return angle;
	}
}

bool Meme_Angle(Droite d1, Droite d2)
{
	return (abs(Angle(d1)-Angle(d2))<0.30);//0.18
}

